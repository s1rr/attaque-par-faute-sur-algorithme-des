import math
import textwrap
from random import choice
# Ma propre implémentation du DES

# ----------------------------------------------------------------------------------------------------

HEX_to_Binary = {'0':'0000', '1':'0001', '2':'0010', '3':'0011', '4':'0100', '5':'0101', '6':'0110', '7':'0111', '8':'1000', '9':'1001', 'A':'1010', 'B':'1011', 'C':'1100', 'D':'1101', 'E':'1110', 'F':'1111'}

def hexDigit_to_binary_bits(hex_digit):
    binary_4bits = HEX_to_Binary[hex_digit]
    return binary_4bits

def hex_str_to_bin(hex_string):
    binary_bits = ""
    for hex_digit in hex_string:
        binary_bits += hexDigit_to_binary_bits(hex_digit)
    return binary_bits

def bin_str_to_hex(bits_string):
    """https://stackoverflow.com/questions/2072351/python-conversion-from-binary-string-to-hexadecimal
    """
    # hex(int(bits_string, 2))[2:]
    bstr = bits_string
    hstr = '%0*X' % ((len(bstr) + 3) // 4, int(bstr, 2))
    return hstr



# ----------------------------------------------------------------------------------------------------

# Key test
# key = hex_str_to_bin("0123456789ABCDEF")
# "6D52CB19541FE34F"
# Message chiffre juste :
# "5772BF9AB7020BD2"
# Message chiffre faux :
# 5567BF9EB7020BC6
# chiffre_faux = hex_str_to_bin("5567BF9EB7020BC6")

message = hex_str_to_bin("45AD37644CE0E0EA")
chiffre = hex_str_to_bin("9A326F737A192F2C")

def encrypt(m, K):
    """m messages 64 bits, K la clé de 64 bits"""
    L = [0] * 16
    R = [0] * 16
    m_permuted = initial_permutation(m)
    L, R = m_permuted[:32], m_permuted[32:]
    subkeys = key_scheduling(K)

    for i in range(16):
        tmp = XOR(L, F(R, subkeys[i]))
        L = R
        R = tmp
    c = R + L # on inverse R et L a la fin
    c = final_permutation(c)
    return c

def decrypt(c, K):
    """m messages 64 bits, K la clé de 64 bits"""
    L = [0] * 16
    R = [0] * 16
    m_permuted = initial_permutation(c)
    L, R = m_permuted[:32], m_permuted[32:]
    subkeys = key_scheduling(K)
    subkeys.reverse()

    for i in range(16):
        tmp = XOR(L, F(R, subkeys[i]))
        L = R
        R = tmp
    c = R + L # on inverse R et L a la fin
    c = final_permutation(c)
    return c


#Final permut for datas after the 16 rounds
IP = [58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4,
      62, 54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16, 8, 57, 49,
      41, 33, 25, 17, 9, 1, 59, 51, 43, 35, 27, 19, 11, 3, 61, 53, 45, 37,
      29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7]

IP_inverse = [40, 8, 48, 16, 56, 24, 64, 32,
              39, 7, 47, 15, 55, 23, 63, 31,
              38, 6, 46, 14, 54, 22, 62, 30,
              37, 5, 45, 13, 53, 21, 61, 29,
              36, 4, 44, 12, 52, 20, 60, 28,
              35, 3, 43, 11, 51, 19, 59, 27,
              34, 2, 42, 10, 50, 18, 58, 26,
              33, 1, 41, 9, 49, 17, 57, 25]

def initial_permutation(m):
    m_permuted = ""
    for i in range(64):
        m_permuted += m[IP[i] - 1]
    return m_permuted

def final_permutation(c):
    c_final = ""
    for i in range(64):
        c_final += c[IP_inverse[i] - 1]
    return c_final

expansion_list = [32, 1, 2, 3, 4, 5,
                  4, 5, 6, 7, 8, 9,
                  8, 9, 10, 11, 12, 13,
                  12, 13, 14, 15, 16, 17,
                  16, 17, 18, 19, 20, 21,
                  20, 21, 22, 23, 24, 25,
                  24, 25, 26, 27, 28, 29,
                  28, 29, 30, 31, 32, 1]


def expansion(R):
    R_48 = ""
    for i in range(48):
        R_48 += R[expansion_list[i] - 1]
    return R_48

def XOR(a, b):
    """XOR between strings"""
    n = len(a)
    res = ""
    for i in range(n):
        res += str((int(a[i]) + int(b[i])) % 2)
    return res

Pbox = [16, 7, 20, 21, 29, 12, 28, 17,
      1, 15, 23, 26, 5, 18, 31, 10,
      2, 8, 24, 14, 32, 27, 3, 9,
      19, 13, 30, 6, 22, 11, 4, 25]

# inverse_permutation(Pbox, 32)
inverse_Pbox = [9, 17, 23, 31, 13, 28, 2, 18, 24, 16, 30, 6, 26, 20, 10, 1, 8, 14, 25, 3, 4, 29, 11, 19, 32, 12, 22, 7, 5, 27, 15, 21]


def F_permutation(S):
    S_permuted = ""
    for i in range(32):
        S_permuted += S[Pbox[i] - 1]
    return S_permuted

def F_permutation_inverse(S):
    S_permuted = ""
    for i in range(32):
        S_permuted += S[inverse_Pbox[i] - 1]
    return S_permuted


def F(R, subkey):
    XOR_48 = XOR(expansion(R), subkey)
    sbox_32 = Sboxes(XOR_48)
    result = F_permutation(sbox_32)
    return result


# R 11001100000000001100110011111111
# subkey 000110110000001011101111111111000111000001110010
# XOR_48 111111101000001011101110100110011110011110001101

# ----------------------------------------------------------------------------------------------------
# SBOXES
SBOX = [
[[14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7],  # Box-1
 [0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8],
 [4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0],
 [15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13]],
[[15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10],  # Box-2
 [3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5],
 [0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15],
 [13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9]],
[[10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8],  # Box-3
 [13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1],
 [13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7],
 [1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12]],
[[7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15],  # Box-4
 [13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9],
 [10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4],
 [3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14]],
[[2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9],  # Box-5
 [14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6],
 [4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14],
 [11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3]],
[[12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11],  # Box-6
 [10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8],
 [9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6],
 [4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13]],
[[4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1],  # Box-7
 [13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6],
 [1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2],
 [6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12]],
[[13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7],  # Box-8
 [1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2],
 [7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8],
 [2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11]]
]

# https://urwithajit9.medium.com/how-to-teach-des-using-python-the-easy-way-part-2-round-function-f-285dd3aef34d
def split_6bits(XOR_48):
    # coupe en 8 parts
    list_6 = [0] * 8
    for i in range(8):
        list_6[i] = XOR_48[6*i: 6*(i + 1)]
    return list_6

def split_4bits(Sbox_32):
    # coupe en 8 parts
    list_4 = [0] * 8
    for i in range(8):
        list_4[i] = Sbox_32[4*i: 4*(i + 1)]
    return list_4


def Sboxes(XOR_48):
    list_6 = split_6bits(XOR_48)
    list_4 = [0] * 8
    # print(list_6)
    for i in range(8):
        ligne = list_6[i][0] + list_6[i][-1]
        ligne = str_bits_to_int(ligne)
        colonne = list_6[i][1:5]
        colonne = str_bits_to_int(colonne)
        # print(ligne, colonne)
        sbox_num = SBOX[i][ligne][colonne]
        # print(sbox_num)
        list_4[i] = dec_to_str_bits(sbox_num)
    sbox_32 = ""
    for i in range(8):
        sbox_32 += list_4[i]
    # print (list_4)
    return sbox_32


def str_bits_to_int(str):
    n = len(str)
    a = 0
    for i in range(n):
        a += int(str[n - 1 - i]) * 2**i
    return a

def dec_to_str_bits(a):
    return bin(a)[2:].zfill(4)


# ----------------------------------------------------------------------------------------------------
# KEY SCHEDULING

PC1_list = [57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36, 63, 55, 47, 39, 31, 23, 15, 7, 62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20, 12, 4]

PC2_list = [14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4, 26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40, 51, 45, 33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32]

shift_list = [1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1]


def key_scheduling(K):
    subkeys = [0] * 16
    C = [0] * 16
    D = [0] * 16
    K_56 = PC1(K) # on ignore les 8*i bits
    K_left, K_right = split_keys(K_56)
    C, D = circular_left_shift(C, D, K_left, K_right)
    for i in range(16):
        subkeys[i] = C[i] + D[i]
        subkeys[i] = PC2(subkeys[i])
    return subkeys

def PC1(K_64):
    K_56 = "" # [0] * 56
    for i in range(56):
        K_56 += K_64[PC1_list[i] - 1]
    return K_56

def split_keys(K_pc1):
    return K_pc1[0:28], K_pc1[28:]

def left_shift(str, bytes):
    str = str[bytes:] + str[:bytes]
    return str

def circular_left_shift(C, D, K_left, K_right):
    C[0] = left_shift(K_left, shift_list[0])
    D[0] = left_shift(K_right, shift_list[0])
    for i in range(1, 16):
        C[i] = left_shift(C[i - 1], shift_list[i])
        D[i] = left_shift(D[i - 1], shift_list[i])
    return C, D

def PC2(K_56):
    K_48 = ""
    for j in range(48):
        K_48 += K_56[PC2_list[j] - 1]
    return K_48



# ----------------------------------------------------------------------------------------------------
# Attaque

liste_chiffres_fautes = ["98376F377A192F38",
 "9A206F337A182F2C",
 "9A226D377A192F2C",
 "9B626B317A192F2C",
 "9B726F3778192F2C",
 "9B726B736A1B2F2C",
 "9B326B737A192D2C",
 "9B726B723A1D2F2E",
 "92726B727A0D2F2C",
 "9A3A6F733A0D2F2C",
 "9A3267727A092F2C",
 "9A327F7A3A092F2D",
 "DA327F72324D2F2C",
 "DA327F737A112F2C",
 "DA327F737A59272D",
 "9A327F737E593F24",
 "FA327F737A193E6C",
 "9A126F737A193F6C",
 "9A324F737E192E6C",
 "8E322F537E192F6C",
 "8E326E735E192E6C",
 "9A322E737A392F2C",
 "9E326E737A190F2C",
 "9E322F637B192F0C",
 "0E322F737A196B2C",
 "9AB26F737B196B2C",
 "9A32EF637B192F2C",
 "9A336FF37B192B3C",
 "9A326F63FB196F38",
 "9A366F737A992F28",
 "9A376F737A19AF2C",
 "9A366F337A182FBC"
]

def attaque_par_faute(m, c, liste_cf):
    list_potential_keys = []
    classement_sbox = classement_sbox_chiffres_fautes(c, liste_cf)
    liste_key_possible = brutforce_sboxes(c, liste_cf, classement_sbox)
    liste_key_juste_6 = choix_cle_juste(liste_key_possible)
    key_48 = ''.join(liste_key_juste_6)
    key_64 = brutforce_bits_manquants(m, c, key_48)
    return key_64

# attaque_par_faute(message, chiffre, liste_chiffres_fautes)

def brutforce_bits_manquants(m, c, key_48):
    missing_bits_k48 = tri_insertion(missing_bits_PC2()) # tri réimplémenté pour l'occasion
    for i in range(2**8):
        random_key_56 = PC2_inverse(key_48[:])
        random_additional_bits = bin(i)[2:].zfill(8) # on génère une liste de 8 bits
        k = 0
        for j in missing_bits_k48: # j parcourt les endroits non traité par PC2
            random_key_56 = random_key_56[:j - 1] + random_additional_bits[k] + random_key_56[j:]            
            k += 1
            
        random_key_64 = PC1_inverse(random_key_56)
        random_key_64 = bits_de_parite(random_key_64)
        chiffre_test = encrypt(m, random_key_64) # Finalement, on vérifie la clé généré
        if chiffre_test == c:
            return random_key_64
    return 0

def inverse_permutation(permu, size):
    res = [0] * size
    for i in range(len(permu)):
        res[permu[i] - 1] = i + 1
    return res

PC2_list = [14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4,
            26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40, 51, 45,
            33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32]

# inverse_PC2_list = inverse_permutation(PC2_list, 56)
inverse_PC2_list = [5, 24, 7, 16, 6, 10, 20, 18, 0, 12, 3, 15, 23, 1,
                    9, 19, 2, 0, 14, 22, 11, 0, 13, 4, 0, 17, 21, 8, 47, 31, 27, 48, 35,
                    41, 0, 46, 28, 0, 39, 32, 25, 44, 0, 37, 34, 43, 29, 36, 38, 45, 33,
                    26, 42, 0, 30, 40]

# inverse_PC1_list = inverse_permutation(PC1_list, 56)
inverse_PC1_list = [8, 16, 24, 56, 52, 44, 36, 0, 7, 15, 23, 55, 51,
                    43, 35, 0, 6, 14, 22, 54, 50, 42, 34, 0, 5, 13, 21, 53, 49, 41, 33, 0,
                    4, 12, 20, 28, 48, 40, 32, 0, 3, 11, 19, 27, 47, 39, 31, 0, 2, 10, 18,
                    26, 46, 38, 30, 0, 1, 9, 17, 25, 45, 37, 29, 0]


def PC2_inverse(K_48):
    res = [0] * 56
    for j in range(56):
        res[j] = K_48[inverse_PC2_list[j] - 1]
    return ''.join(res)

def PC1_inverse(K_56):
    res = [0] * 64
    for j in range(64):
        res[j] = K_56[inverse_PC1_list[j] - 1]
    return ''.join(res)

def tri_insertion(liste):
    for i in range(1, len(liste)):
        clef = liste[i]
        j = i - 1
        while j >= 0 and liste[j] > clef:
            liste[j + 1] = liste[j]
            j -= 1
        liste[j + 1] = clef
    return liste

def missing_bits_PC2():
    full_bits = [ i for i in range(1, 57) ]
    return list(set(full_bits) - set(PC2_list))


def bits_de_parite(key_64):    
    key = ''
    parite = [0] * 8

    for i in range(8):
        substr = key_64[8 * i : 8 * (i + 1) - 1]
        for j in range(7):
            parite[i] += int(substr[j])
            if parite[i] % 2 == 0:
                parite[i] = 1
            else:
                parite[i] = 0
    for i in range(8):
        key += key_64[8 * i : 8 * (i + 1) - 1] + str(parite[i])
    return key

def choix_cle_juste(liste_key_possible):
    """https://stackoverflow.com/questions/10066642/how-to-find-common-elements-in-list-of-lists
    On utilise le type set, et on fait l'intersection entre les
    ensembles de cles possible correspondant aux chiffres fautes
    """
    result = []
    # print(liste_key_possible)
    for i in range(len(liste_key_possible)):
        ensemble = set(liste_key_possible[i][0])
        for s in liste_key_possible[i][1:]:
            ensemble.intersection_update(s)
        result.append(list(ensemble)[0])
    return result


def brutforce_sboxes(c, liste_cf, classement_sbox):
    calcul = [0] * 48
    liste_cle_possible = [ [] for j in range(8) ]

    c_permuted = initial_permutation(c)
    L, R = c_permuted[:32], c_permuted[32:]

    for i in range(8): # on parcourt les Sboxes
        for j in classement_sbox[i]: # on parcourt les chiffres correspondant
            liste_cle_chiffres_fautes = []
            cf = hex_str_to_bin(liste_cf[j])
            cf_permuted = initial_permutation(cf)
            Lf, Rf = cf_permuted[:32], cf_permuted[32:]
            e = XOR(R, Rf)
            e_prime = F_permutation_inverse(XOR(L, Lf))

            for k in range(64): # on test toutes les clés possibles
                random_key_48, random_key_6 = generate_key_bruteforce_sboxes(i, k)
                F1 = Sboxes(XOR(expansion(R), random_key_48))
                F2 = Sboxes(XOR(expansion(Rf), random_key_48))
                calcul = XOR(F1, F2)
                if e_prime[4 * i : 4 * (i + 1)] == calcul[4 * i : 4 * (i + 1)]: # on test sur la portion sbox
                    liste_cle_chiffres_fautes.append(random_key_6)
            liste_cle_possible[i].append(liste_cle_chiffres_fautes)
    return liste_cle_possible


def generate_key_bruteforce_sboxes(sbox, k):
    random_key_6 = bin(k)[2:].zfill(6)
    random_key = '0' * (sbox * 6) + random_key_6 + '0' * (48 - 6 * (sbox + 1))
    return random_key, random_key_6


def classement_sbox_chiffres_fautes(c, liste_chiffres_fautes):
    liste_classement = [ [] for j in range(8) ]

    c_permuted = initial_permutation(c)
    R = c_permuted[32:]

    for i in range(len(liste_chiffres_fautes)):
        liste_sboxes = []
        cf_permuted = initial_permutation(hex_str_to_bin(liste_chiffres_fautes[i]))
        Rf = cf_permuted[32:]
        Re, Rfe = expansion(R), expansion(Rf)

        # e = XOR(R, Rf)
        e_expanded = XOR(Re, Rfe)
        for j in range(len(e_expanded)):
            if e_expanded[j] == '1':
                liste_classement[j // 6].append(i)

    return liste_classement

# ----------------------------------------------------------------------------------------------------

def liste_cle_tous_etudiant():
    resultat = []
    for i in range(42):
        res = []
        m = hex_str_to_bin(liste_etudiant[i][0])
        c = hex_str_to_bin(liste_etudiant[i][1])
        liste_cf = liste_etudiant[i][2]
        cle = attaque_par_faute(m, c, liste_cf)
        res.append(nom_etudiant[i])
        res.append(bin_str_to_hex(cle))
        resultat.append(res)
    return resultat
